import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';


class FormPakar extends StatefulWidget {
  @override
  _FormPakar createState() => _FormPakar();
}

class _FormPakar extends State<FormPakar> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff222831),
        title: Text("Pendaftaran Tim Pakar Covid-19 Indonesia",
         style: GoogleFonts.poppins(
            textStyle: TextStyle(color: Colors.white, letterSpacing: .5, fontWeight: FontWeight.bold),
          ),),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "contoh: Susilo Bambang",
                      labelText: "Nama Lengkap",
                      icon: const Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Nama tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "contoh: Rumah Sakit",
                      labelText: "Tempat Bekerja",
                      icon: const Icon(Icons.domain_sharp),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Tempat Bekerja tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(  
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "contoh: Universitas Indonesia",
                      labelText: "Asal Universitas",
                      icon: const Icon(Icons.school),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Universitas tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(  
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: "Deskripsi Diri",
                      icon: const Icon(Icons.assignment_ind),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return 'Deskripsi diri tidak boleh kosong';
                      }
                      return null;
                    },
                  ),
                ),
              RaisedButton(
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  print("Submit Button");
                  },
              ),
            ],
          ),
        ),
      ),
    ));
  }
}