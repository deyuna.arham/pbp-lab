1. Apakah perbedaan antara JSON dan XML?
Pertama-tama pengertian dari JSON (Javascript Object Notation) sendiri adalah format untuk pertukaran data di berbagai platform, sedangkan pengertian dari XML (eXtensible Markup Language) adalah bahasa markup yang digunakan untuk menyimpan data. Perbedaan dari JSON dan XML diantaranya adalah:
    1. Tipe JSON adalah bahasa meta, sedangkan XML adalah bahasa markup
    2. Kompleksitas JSON lebih sederhana dan mudah dibaca, sedangkan XML lebih rumit
    3. Orientasi JSON adalah pada data, sedangkan orientasi XML adalah pada dokumen
    4. JSON mendukung array, sedangkan XML tidak mendukung array
    5. Ditinjau dari ekstensi file: JSON diakhiri dengan ekstensi .json sedangkan XML diakhiri dengan ekstensi .xml
    6. Ditinjau dari penggunaan tag: JSON tidak menggunakan tag sedangkan XML menggunakan tag pada awal dan akhir
    7. JSON tidak mendukung penggunaan comment, sedangkan XML mendukung penggunaan comment
    8. File JSON sizenya cenderung lebih kecil, sedangkan XML cenderung lebih besar
    9. Penyimpanan data JSON berada dalam bentuk seperti map dengan key dan value, sedangkan XML disimpan dalam bentuk tree structure
    10. JSON mendukung UTF serta ASCII, sedangkan XML mendukung UTF-8 dan UTF-16
    11. JSON dapat melakukan pemrosesan dan pemformatan dokumen apapun, sedangkan XML tidak dapat melakukan pemrosesan atau perhitungan apapun
    12. Dilihat dari dukungan namespaces: JSON mendukung namespaces, komentar, dan metadata, sedangkan XML tidak ada ketentuan untuk namespaces, komentar, dan metadata
    13. JSON aman hampir sepanjang waktu (kecuali JSON tersebut digunakan karena dapat menyebabkan serangan Cross-site Request), sedangkan XML lebih rentan terhadap beberapa serangan karena aktifasi perluasan entitas eksternal dan validasi DTD adalah secara default.

2. Apakah perbedaan antara HTML dan XML?
Pertama-tama pengertian dari HTML (Hypertext Markup Language) adalah bahasa markup yang berfungsi untuk menyusun halaman website, sedangkan sama seperti nomor sebelumnya pengertian dari XML adalah bahasa markup yang digunakan untuk menyimpan data. Perbedaan dari HTML dan XML diantaranya adalah:
    1. Fokus dari HTML adalah pada penyajian dari sebuah data, sedangkan XML pada transfer data
    2. HTML case insensitive, sedangkan XML case sensitive
    3. HTML tidak menyajikan dukungan namespaces, sedangkan XML menyediakan dukungan namespaces
    4. HTML tidak strict pada tag penutup, sedangkan XML strict pada tag penutup
    5. HTML memiliki tag yang terbatas, sedangkan XML masih dapat dikembangkan
    6. HTML tidak mempertahankan White spaces, sedangkan XML bisa mempertahankan white spaces 
    7. HTML adalah presentasion-driven, sedangkan XML content-driven dan tidak banyak fitur performatan yang tersedia
    8. Kesalahan kecil pada output HTML masih bisa dihiraukan, sedangkan kesalahan pada XML tidak bisa dijadikan final output
    9. HTML memiliki size file yang cenderung kecil, sedangkan XML cenderung besar
    10. HTML tidak mengharuskan end tag, sedangkan end tag sangat penting dalam dokumen

Referensi: 
- https://www.niagahoster.co.id/blog/json-adalah/
- https://id.sawakinome.com/articles/protocols--formats/difference-between-json-and-xml-3.html
- https://blogs.masterweb.com/perbedaan-xml-dan-html/