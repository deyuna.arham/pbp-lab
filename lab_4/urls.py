from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('add-note', views.add_note),
    path('note-list', views.note_list)
]