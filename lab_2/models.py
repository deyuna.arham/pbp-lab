from django.db import models

# Create your models here.
class Note(models.Model):
    modelTo = models.TextField()
    modelFrom = models.TextField()
    title = models.TextField()
    message = models.TextField()