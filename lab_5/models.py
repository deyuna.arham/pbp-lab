from django.db import models

# Create your models here.
class Note(models.Model):
    modelTo = models.CharField(max_length=30)
    modelFrom = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.TextField()